import java.util.Random;
import java.util.Scanner;

public class Sulifa {
    public static String computersTurn(String[] variants){

        Random random = new Random();
        int turn = random.nextInt(0,3);

        return variants[turn];
    }
    //equals - computersChoice == ножницы
    public static void findWinner( String computersChoice, String myChoice){
        if((computersChoice.equals("ножницы") && myChoice.equals("бумага")) || (computersChoice.equals("бумага") && myChoice.equals("камень")) || (computersChoice.equals("камень") && myChoice.equals("ножница"))){
            System.out.println("Победил компьютер!");
        }else if(computersChoice.equals(myChoice)){
            System.out.println("Ничья!");
        }else{
            System.out.println("Мы победили!");
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String[] variants = {"бумага","ножницы","камень"};

        String computersChoice = computersTurn(variants);

        int myTurn = scanner.nextInt();

        while (myTurn > 2 || myTurn < 0 ){
            myTurn = scanner.nextInt();
        }

        String myChoice = variants[myTurn];

        findWinner(computersChoice, myChoice);
        System.out.println("Мой выбор " + myChoice);
        System.out.println("Выбор компьютера " + computersChoice);
    }
}
